import React from 'react';
import { Container, Row, Col, Card } from'react-bootstrap';

const About = () => {
    return (
        <Container className='mt-3'>
            <h2 className='text-center mb-4'>About Us</h2>
            <Row className='justify-content-center'>
                <Col md={8}>
                    <Card>
                        <Card.Body>
                            <Card.Title>About Us</Card.Title>
                            <Card.Text>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. 
                                Sed nisi. Nulla quis sem at nibh elementum imperdiet. 
                                Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default About;