import React, { useState } from 'react';
import { Container, Row, Col, Card, Carousel, Modal } from 'react-bootstrap';

const Project = () => {
    // Image and video data
    const imageData = [{ src: '../assets/aman1.jpeg', caption: 'Image 1 Caption', description: 'Description for Image 1' }, { src: '../assets/aman2.jpeg', caption: 'Image 2 Caption', description: 'Description for Image 2' }, { src: '../assets/aman3.jpeg', caption: 'Image 3 Caption', description: 'Description for Image 3' }];
    const videoData = [{ src: '../assets/aman-video1.mp4', caption: 'Video 1 Caption', description: 'Description for Video 1' }, { src: '../assets/aman-video2.mp4', caption: 'Video 2 Caption', description: 'Description for Video 2' }, { src: '../assets/aman-video3.mp4', caption: 'Video 3 Caption', description: 'Description for Video 3' }];



    // State for controlling modal visibility and selected media
    const [showModal, setShowModal] = useState(false);
    const [selectedMedia, setSelectedMedia] = useState(null);

    // Function to handle image or video click
    const handleMediaClick = (media) => {
        console.log("uuuuuuuuuuuuuuuuuuuu")
        setSelectedMedia(media);
        setShowModal(true);
    };

    return (
        <Container className='mt-3'>
            <h2 className="text-center mb-4">Projects</h2>
            <Row xs={1} md={2} lg={2} className="g-4">
                {/* Image Carousel Card */}
                <Col className='d-flex'>
                    <Card className='w-100 h-100 d-flex flex-column'>
                        <Card.Body>
                            <Carousel>
                                {imageData.map((image, index) => (
                                    <Carousel.Item key={index} onClick={() => handleMediaClick(image)}>
                                        <img
                                            className="d-block w-100"
                                            src={image.src}
                                            alt={`Image ${index}`}
                                        />
                                        <Carousel.Caption>
                                            <h3>{image.caption}</h3>
                                            <p>{image.description}</p>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                ))}

                                {/* Add more Carousel.Items with other images */}
                            </Carousel>
                        </Card.Body>
                    </Card>
                </Col>
                {/* Video Carousel Card */}
                <Col className='d-flex'>
                    <Card className='w-100 h-100 d-flex flex-column'>
                        <Card.Body className="d-flex flex-column align-items-center justify-content-center">
                            <Carousel style={{ width: '100%' }}>
                                {videoData.map((video, index) => (
                                    <Carousel.Item key={index} onClick={() => handleMediaClick(video)}>
                                        <video width="100%" controls>
                                            <source src={video.src} type="video/mp4" />
                                            Your browser does not support the video tag.
                                        </video>
                                        <Carousel.Caption>
                                            <h3>{video.caption}</h3>
                                            <p>{video.description}</p>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                ))}

                                {/* Add more Carousel.Items with other videos */}
                            </Carousel>
                        </Card.Body>
                    </Card>
                </Col>

            </Row>

            {/* Modal for zooming in */}
            <Modal show={showModal} onHide={() => setShowModal(false)} size="xl">
                <Modal.Body className="text-center">
                    {selectedMedia && (
                        selectedMedia.src.endsWith('.jpeg') || selectedMedia.src.endsWith('.png') || selectedMedia.src.endsWith('.jpg') ? (
                            <img src={selectedMedia.src} alt="Zoomed Image" style={{ maxWidth: '100%', maxHeight: '80vh' }} />
                        ) : (
                            <video width="100%" controls>
                                <source src={selectedMedia.src} type="video/mp4" />
                                Your browser does not support the video tag.
                            </video>
                        )
                    )}
                </Modal.Body>
            </Modal>
        </Container>
    )
}

export default Project;
