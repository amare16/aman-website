import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { animateScroll as scroll } from 'react-scroll';

const CustomNavbar = () => {
    const scrollToTop = () => {
        scroll.scrollToTop();
    };
    return (
        <Navbar bg="primary" expand="lg" variant="dark">
            <Container>
                <Navbar.Brand as={Link} to="/" onClick={scrollToTop}>
                    <img
                        src="../assets/aman-logo.png"
                        height="50"
                        className="d-inline-block align-top"
                        alt="Logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                    <Nav className="ms-auto" style={{fontWeight: 'bold', fontSize: '20px'}}>
                        <Nav.Link as={Link} to="/" onClick={scrollToTop} className='text-white'>Home</Nav.Link>
                        <Nav.Link as={Link} to="/about" className='text-white'>About</Nav.Link>
                        <Nav.Link as={Link} to="/project" className='text-white'>Project</Nav.Link>
                        <Nav.Link as={Link} to="/contact" className='text-white'>Contact</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default CustomNavbar;