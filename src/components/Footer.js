import React from 'react';
import { Container } from 'react-bootstrap';

const Footer = () => {
    return (
        <footer className="bg-primary text-light fixed-bottom">
            <Container className="py-3">
                <div className="text-center">
                    &copy; 2024 <strong>Amanuel Peinture Applicateur Revetement de Sol.</strong> All rights reserved.
                </div>
            </Container>
        </footer>
    );
};

export default Footer;
