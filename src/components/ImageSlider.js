import React from "react";
import { Carousel } from "react-bootstrap";

const ImageSlider = () => {
    return (
        <Carousel>
            <Carousel.Item>
                <video
                    controls
                    className="d-block w-100"
                    style={{ height: "75vh" }} 
                    src="../assets/aman-video1.mp4"
                    typeof="video/mp4"
                />
                <Carousel.Caption>
                    <h3>First video</h3>
                    <p>Description of the first video.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    style={{ height: "75vh" }} 
                    src="../assets/aman1.jpeg"
                    alt="First slide"
                />
                <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="../assets/aman2.jpeg"
                    style={{ height: "75vh" }} 
                    alt="Second slide"
                />
                <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="../assets/aman3.jpeg"
                    alt="Third slide"
                    style={{ height: "75vh" }} 
                />
                <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="../assets/aman4.jpeg"
                    alt="Fourth slide"
                    style={{ height: "75vh" }} 
                />
                <Carousel.Caption>
                    <h3>Fourth slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            {/* Add more Carousel.Item for additional images */}
        </Carousel>
    );
}

export default ImageSlider;