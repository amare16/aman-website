import React, { useRef } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import emailjs from '@emailjs/browser';
import { ToastContainer, toast } from'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const Contacts = () => {
    const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm('service_df8j6ct', 'template_pd37t8r', form.current, {
        publicKey: 'gXOT-NFAsUraSssUX',
      })
      .then(
        () => {
            toast.success('🦄 Formulaire soumis avec succès!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
                });
        },
        (error) => {
            toast.error('🦄 Veuillez fournir une valeur dans chaque champ de saisie!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
                });
        },
      );
  };
    

    return (
        <Container className='mt-3'>
            <h2 className='text-center mb-4'>Contactez-nous</h2>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <Row className='mb-4'>
                <Col md={6}>
                    <Row>
                        <Col md={2}></Col>
                        <Col md={8} style={{backgroundColor: '#0d6efd', paddingTop: '10px', paddingBottom: '10px', borderRadius: '10px'}}>
                            <h3 className='mb-3 text-center text-white'>Envoie-nous un message</h3>
                            <Form ref={form} onSubmit={sendEmail}>
                                <Form.Group className='mb-3' controlId="nom">
                                    <Form.Control
                                        type="text"
                                        placeholder="Nom"
                                        name="from_name"
                                        required
                                    />
                                </Form.Group>
                                <Form.Group className='mb-3' controlId="email">
                                    <Form.Control
                                        type="email"
                                        placeholder="Email"
                                        name="from_email"
                                        required
                                    />
                                </Form.Group>
                                <Form.Group className='mb-3' controlId="message">
                                    <Form.Control
                                        as="textarea"
                                        rows={4}
                                        placeholder="Message"
                                        name="message"
                                        required
                                    />
                                </Form.Group>
                                <Button style={{ backgroundColor: '#ff007f' }} type="submit">
                                    Submit
                                </Button>
                            </Form>
                        </Col>
                        <Col md={2}></Col>
                    </Row>

                </Col>
                <Col md={6}>
                    <Row>
                        <Col md={2}></Col>
                        <Col md={8}>
                            <div className="d-flex align-items-center justify-content-center" style={{ height: '400px', borderRadius: '10px', overflow: 'hidden' }}>
                                <img
                                    src="../assets/Aman-business-card.jpg"
                                    alt="Contact Map"
                                    style={{ width: '100%', height: 'auto' }}
                                />
                            </div>
                        </Col>
                        <Col md={2}></Col>
                    </Row>

                </Col>
            </Row>
        </Container>
    );
}

export default Contacts;