import React from 'react';
import { Container, Row, Col } from'react-bootstrap';
import ImageSlider from './ImageSlider.js';

const Home = () => {
    return (
        <div>
            <Container style={{ height: "75vh", marginTop: "1.4rem"}}>
                <Row>
                    <Col sm={2}></Col>
                    <Col sm={8}>
                        <ImageSlider />
                    </Col>
                    <Col sm={2}></Col>
                </Row>
            </Container>
        </div>
    );
}

export default Home;